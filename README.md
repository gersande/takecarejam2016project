**Take Care Jam 2016 Project**

This is a Twine game made for the [Take Care Jam](https://twitter.com/search?q=%23takecarejam&src=typd)

**Jam Team Members:**

* [Sarah Ganzon](https://twitter.com/sarah_ganzon)
* [Carolyn Jong](https://twitter.com/ckjong)
* Gersande

**Interested in Playing?**

*Warning: The game is still in progress and contains bugs and other oddities.*

The reason the game exists here is because I wanted to be able to use **version control** to backup the work-in-progress of the game. 

If you just want to download the *repository* folder in order to find the HTML file you can run in your browser to play the twine game, here are some instructions:

<img src="http://d.pr/i/1ar4J+" style="width:500px;">

<img src="http://d.pr/i/1iMFa+" style="width:500px;">

<img src="http://d.pr/i/8erz+" style="width:500px;">
